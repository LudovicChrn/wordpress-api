<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              icinonplus
 * @since             1.0.0
 * @package           Apitrombi
 *
 * @wordpress-plugin
 * Plugin Name:       APITrombi
 * Plugin URI:        jaipas
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Ludovic
 * Author URI:        icinonplus
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       apitrombi
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'APITROMBI_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-apitrombi-activator.php
 */
function activate_apitrombi() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-apitrombi-activator.php';
	Apitrombi_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-apitrombi-deactivator.php
 */
function deactivate_apitrombi() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-apitrombi-deactivator.php';
	Apitrombi_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_apitrombi' );
register_deactivation_hook( __FILE__, 'deactivate_apitrombi' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-apitrombi.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_apitrombi() {

	$plugin = new Apitrombi();
	$plugin->run();

}
run_apitrombi();

/* Créer CPT & Taxo */

function cptui_register_my_cpts() {

	/**
	 * Post Type: Apprenants.
	 */

	$labels = [
		"name" => __( "Apprenants", "twentytwentyone" ),
		"singular_name" => __( "Apprenant", "twentytwentyone" ),
		"menu_name" => __( "Apprenants", "twentytwentyone" ),
		"all_items" => __( "Tous les Apprenants", "twentytwentyone" ),
		"add_new" => __( "Ajouter un nouveau", "twentytwentyone" ),
		"add_new_item" => __( "Ajouter un nouveau Apprenant", "twentytwentyone" ),
		"edit_item" => __( "Modifier Apprenant", "twentytwentyone" ),
		"new_item" => __( "Nouveau Apprenant", "twentytwentyone" ),
		"view_item" => __( "Voir Apprenant", "twentytwentyone" ),
		"view_items" => __( "Voir Apprenants", "twentytwentyone" ),
		"search_items" => __( "Recherche de Apprenants", "twentytwentyone" ),
		"not_found" => __( "Aucun Apprenants trouvé", "twentytwentyone" ),
		"not_found_in_trash" => __( "Aucun Apprenants trouvé dans la corbeille", "twentytwentyone" ),
		"parent" => __( "Parent Apprenant :", "twentytwentyone" ),
		"featured_image" => __( "Image mise en avant pour ce Apprenant", "twentytwentyone" ),
		"set_featured_image" => __( "Définir l’image mise en avant pour ce Apprenant", "twentytwentyone" ),
		"remove_featured_image" => __( "Retirer l’image mise en avant pour ce Apprenant", "twentytwentyone" ),
		"use_featured_image" => __( "Utiliser comme image mise en avant pour ce Apprenant", "twentytwentyone" ),
		"archives" => __( "Archives de Apprenant", "twentytwentyone" ),
		"insert_into_item" => __( "Insérer dans Apprenant", "twentytwentyone" ),
		"uploaded_to_this_item" => __( "Téléverser sur ce Apprenant", "twentytwentyone" ),
		"filter_items_list" => __( "Filtrer la liste de Apprenants", "twentytwentyone" ),
		"items_list_navigation" => __( "Navigation de liste de Apprenants", "twentytwentyone" ),
		"items_list" => __( "Liste de Apprenants", "twentytwentyone" ),
		"attributes" => __( "Attributs de Apprenants", "twentytwentyone" ),
		"name_admin_bar" => __( "Apprenant", "twentytwentyone" ),
		"item_published" => __( "Apprenant publié", "twentytwentyone" ),
		"item_published_privately" => __( "Apprenant publié en privé.", "twentytwentyone" ),
		"item_reverted_to_draft" => __( "Apprenant repassés en brouillon.", "twentytwentyone" ),
		"item_scheduled" => __( "Apprenant planifié", "twentytwentyone" ),
		"item_updated" => __( "Apprenant mis à jour.", "twentytwentyone" ),
		"parent_item_colon" => __( "Parent Apprenant :", "twentytwentyone" ),
	];

	$args = [
		"label" => __( "Apprenants", "twentytwentyone" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "apprenant", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail" ],
		"show_in_graphql" => false,
	];

	register_post_type( "apprenant", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );

function cptui_register_my_taxes() {

	/**
	 * Taxonomy: promotions.
	 */

	$labels = [
		"name" => __( "promotions", "twentytwentyone" ),
		"singular_name" => __( "promotion", "twentytwentyone" ),
		"menu_name" => __( "promotions", "twentytwentyone" ),
		"all_items" => __( "Tous les promotions", "twentytwentyone" ),
		"edit_item" => __( "Modifier promotion", "twentytwentyone" ),
		"view_item" => __( "Voir promotion", "twentytwentyone" ),
		"update_item" => __( "Mettre à jour le nom de promotion", "twentytwentyone" ),
		"add_new_item" => __( "Ajouter un nouveau promotion", "twentytwentyone" ),
		"new_item_name" => __( "Nom du nouveau promotion", "twentytwentyone" ),
		"parent_item" => __( "Parent dpromotion", "twentytwentyone" ),
		"parent_item_colon" => __( "Parent promotion :", "twentytwentyone" ),
		"search_items" => __( "Recherche de promotions", "twentytwentyone" ),
		"popular_items" => __( "promotions populaires", "twentytwentyone" ),
		"separate_items_with_commas" => __( "Séparer les promotions avec des virgules", "twentytwentyone" ),
		"add_or_remove_items" => __( "Ajouter ou supprimer des promotions", "twentytwentyone" ),
		"choose_from_most_used" => __( "Choisir parmi les promotions les plus utilisés", "twentytwentyone" ),
		"not_found" => __( "Aucun promotions trouvé", "twentytwentyone" ),
		"no_terms" => __( "Aucun promotions", "twentytwentyone" ),
		"items_list_navigation" => __( "Navigation de liste de promotions", "twentytwentyone" ),
		"items_list" => __( "Liste de promotions", "twentytwentyone" ),
		"back_to_items" => __( "Retourner à promotions", "twentytwentyone" ),
	];

	
	$args = [
		"label" => __( "promotions", "twentytwentyone" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'promotion', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "promotion",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "promotion", [ "apprenant" ], $args );

	/**
	 * Taxonomy: compétences.
	 */

	$labels = [
		"name" => __( "compétences", "twentytwentyone" ),
		"singular_name" => __( "compétence", "twentytwentyone" ),
		"menu_name" => __( "compétences", "twentytwentyone" ),
		"all_items" => __( "Tous les compétences", "twentytwentyone" ),
		"edit_item" => __( "Modifier compétence", "twentytwentyone" ),
		"view_item" => __( "Voir compétence", "twentytwentyone" ),
		"update_item" => __( "Mettre à jour le nom de compétence", "twentytwentyone" ),
		"add_new_item" => __( "Ajouter un nouveau compétence", "twentytwentyone" ),
		"new_item_name" => __( "Nom du nouveau compétence", "twentytwentyone" ),
		"parent_item" => __( "Parent dcompétence", "twentytwentyone" ),
		"parent_item_colon" => __( "Parent compétence :", "twentytwentyone" ),
		"search_items" => __( "Recherche de compétences", "twentytwentyone" ),
		"popular_items" => __( "compétences populaires", "twentytwentyone" ),
		"separate_items_with_commas" => __( "Séparer les compétences avec des virgules", "twentytwentyone" ),
		"add_or_remove_items" => __( "Ajouter ou supprimer des compétences", "twentytwentyone" ),
		"choose_from_most_used" => __( "Choisir parmi les compétences les plus utilisés", "twentytwentyone" ),
		"not_found" => __( "Aucun compétences trouvé", "twentytwentyone" ),
		"no_terms" => __( "Aucun compétences", "twentytwentyone" ),
		"items_list_navigation" => __( "Navigation de liste de compétences", "twentytwentyone" ),
		"items_list" => __( "Liste de compétences", "twentytwentyone" ),
		"back_to_items" => __( "Retourner à compétences", "twentytwentyone" ),
	];

	
	$args = [
		"label" => __( "compétences", "twentytwentyone" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'competence', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "competence",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "competence", [ "apprenant" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes' );

/* add_filter( 'rest_authentication_errors', function( $result ) {
    // If a previous authentication check was applied,
    // pass that result along without modification.
    if ( true === $result || is_wp_error( $result ) ) {
        return $result;
    }
 
    // No authentication has been performed yet.
    // Return an error if user is not logged in.
    if ( ! is_user_logged_in() ) {
        return new WP_Error(
            'rest_not_logged_in',
            __( 'You are not currently logged in.' ),
            array( 'status' => 401 )
        );
    }
 
    // Our custom authentication check should have no effect
    // on logged-in requests
    return $result;
}); */

/* Création ACF */

add_action('add_meta_boxes','init_metabox');
function init_metabox(){
  add_meta_box('url_link', 'Liens', 'url_link', 'apprenant');
}

function url_link($post){
  $linkedin = get_post_meta($post->ID,'_url_linkedin',true);
  $portfolio = get_post_meta($post->ID,'_url_portfolio',true);
  $cv = get_post_meta($post->ID,'_url_cv',true);
  $extrait = get_post_meta($post->ID,'_extrait',true);

  echo '<label for="extrait">Extrait</label></br>';
  echo '<textarea id="extrait" name="extrait">' . $extrait . '</textarea></br>';

  echo '<label class="test" for="url_link">Linkedin</label></br>';
  echo '<input id="url_link" type="url" name="url_linkedin" value="' . $linkedin . '" /></br>';

  echo '<label for="url_port">Portfolio</label></br>';
  echo '<input id="url_port" type="url" name="url_portfolio" value="' . $portfolio . '" /></br>';

  echo '<label for="url_cv">CV</label></br>';
  echo '<input id="url_cv" type="url" name="url_cv" value="' . $cv . '" />';


}

function save_metabox($post_id){
if(isset($_POST['url_linkedin']) && isset($_POST['url_portfolio']) && isset($_POST['url_cv']) && isset($_POST['extrait'])) {
  	update_post_meta($post_id, '_url_linkedin', esc_url($_POST['url_linkedin']));
	update_post_meta($post_id, '_url_portfolio', esc_url($_POST['url_portfolio']));
	update_post_meta($post_id, '_url_cv', esc_url($_POST['url_cv']));
	update_post_meta($post_id, '_extrait', esc_html($_POST['extrait']));
}
}
add_action('save_post','save_metabox');


/* Affichage ACF dans JSON */

function acf_to_rest_api($response, $post, $request) {
    if (!function_exists('url_link')) return $response;

    if (isset($post)) {
        $acf = get_post_meta($post->ID);
		$thumbnailUrl = get_the_post_thumbnail_url($post->ID);
        $response->data['acf'] = $acf;
		$response->data['url'] = $thumbnailUrl;
    }
    return $response;
}
add_filter('rest_prepare_apprenant', 'acf_to_rest_api', 10, 3);

/* Charger le css */

function your_namespace() {
    wp_register_style('style', plugins_url('style.css',__FILE__ ));
    wp_enqueue_style('style');
}

add_action( 'admin_init','your_namespace');
