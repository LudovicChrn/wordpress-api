<?php

/**
 * Fired during plugin activation
 *
 * @link       icinonplus
 * @since      1.0.0
 *
 * @package    Apitrombi
 * @subpackage Apitrombi/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Apitrombi
 * @subpackage Apitrombi/includes
 * @author     Ludovic <charronludovic.26@gmail.com>
 */
class Apitrombi_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
