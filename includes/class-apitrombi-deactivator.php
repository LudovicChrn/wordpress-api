<?php

/**
 * Fired during plugin deactivation
 *
 * @link       icinonplus
 * @since      1.0.0
 *
 * @package    Apitrombi
 * @subpackage Apitrombi/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Apitrombi
 * @subpackage Apitrombi/includes
 * @author     Ludovic <charronludovic.26@gmail.com>
 */
class Apitrombi_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
